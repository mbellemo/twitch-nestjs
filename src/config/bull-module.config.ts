import { BullModuleOptions } from "nest-bull";

export const bullModuleOptions: BullModuleOptions = {
    options: {
        redis: process.env.REDIS_URL || 'redis://localhost:6379'
    }
};