import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';

export const UserSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  salt: { type: String, required: true },
  isActive: { type: Boolean, default: false },
});

UserSchema.methods.validatePassword = async function (password: String): Promise<boolean> {
  const hash = await bcrypt.hash(password, this.salt);
  return hash === this.password;
}