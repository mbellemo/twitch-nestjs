import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { FindUserDto } from './dto/find-user.dto';
import { SearchUserDto } from './dto/search-user.dto';
import { User } from './user.interface';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) { }

  @Post()
  @UsePipes(ValidationPipe)
  async createUser(@Body() createUserDto: CreateUserDto): Promise<User> {
    return await this.usersService.createUser(createUserDto);
  }

  @Get()
  @UsePipes(ValidationPipe)
  async getUsers(@Query() searchUserDto: SearchUserDto): Promise<User[]> {
    return await this.usersService.getUsers(searchUserDto);
  }

  @Get('/:id')
  @UsePipes(ValidationPipe)
  async getUserById(@Param('id') findUserDto: FindUserDto): Promise<User> {
    return await this.usersService.getUserById(findUserDto);
  }

  @Delete('/:id')
  @UsePipes(ValidationPipe)
  async deleteUserById(@Param('id') findUserDto: FindUserDto): Promise<User> {
    return await this.usersService.deleteUserById(findUserDto);
  }
}
