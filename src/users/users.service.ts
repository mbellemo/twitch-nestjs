import {
  Injectable,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { FindUserDto } from './dto/find-user.dto';
import { SearchUserDto } from './dto/search-user.dto';
import { User } from './user.interface';
import { UsersRepository } from './users.repository';

import * as bcrypt from 'bcrypt'

@Injectable()
export class UsersService {
  constructor(private readonly usersRepository: UsersRepository) { }

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    return await this.usersRepository.createUser(createUserDto);
  }

  async getUsers(searchUserDto: SearchUserDto): Promise<User[]> {
    return await this.usersRepository.getUsers(searchUserDto);
  }

  async getUserById(findUserDto: FindUserDto): Promise<User> {
    return await this.usersRepository.getUserById(findUserDto);
  }

  async deleteUserById(findUserDto: FindUserDto): Promise<User> {
    return await this.usersRepository.deleteUserById(findUserDto);
  }
}
