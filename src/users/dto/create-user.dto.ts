import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  username: String;

  @IsNotEmpty()
  password: String;
}
