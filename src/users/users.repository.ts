import { Injectable, NotFoundException, BadRequestException, ConflictException } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from "./dto/create-user.dto";
import { FindUserDto } from "./dto/find-user.dto";
import { SearchUserDto } from "./dto/search-user.dto";
import { User } from './user.interface';

import * as bcrypt from 'bcrypt'

@Injectable()
export class UsersRepository {
    constructor(@InjectModel('User') private readonly userModel: Model<User>) { }

    async createUser(createUserDto: CreateUserDto): Promise<User> {
        const user = new this.userModel(createUserDto);
        user.salt = await bcrypt.genSalt();
        user.password = await this.hashPassword(user.password, user.salt);
        try {
            return await user.save();
        } catch (error) {
            if (error.code === 11000) {
                throw new ConflictException('Username already exists');
            } else {
                throw new BadRequestException(error.message);
            }
        }
    }

    async getUsers(searchUserDto: SearchUserDto): Promise<User[]> {
        return await this.userModel.find(searchUserDto).exec();
    }

    async getUserById(findUserDto: FindUserDto): Promise<User> {
        const found = await this.userModel.findById(findUserDto).exec();
        if (!found) {
            throw new NotFoundException();
        }
        return found;
    }

    async getUserByUsername(username: String): Promise<User> {
        return await this.userModel.findOne({ username }).exec();
    }

    async deleteUserById(findUserDto: FindUserDto): Promise<User> {
        const found = await this.userModel.findOneAndRemove({ _id: findUserDto }).exec();
        if (!found) {
            throw new NotFoundException();
        }
        return found;
    }

    async validateUsernamePassword(createUserDto: CreateUserDto): Promise<String> {
        const { username, password } = createUserDto;

        const found = await this.userModel.findOne({ username }).exec();

        if (found && await found.validatePassword(password)) {
            return found.username;
        } else {
            return null;
        }
    }

    private async hashPassword(password: String, salt: String): Promise<String> {
        return bcrypt.hash(password, salt);
    }
}