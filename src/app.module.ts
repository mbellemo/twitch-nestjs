import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { StreamsModule } from './streams/streams.module';
import { UsersModule } from './users/users.module';
import { mongooseConfig } from './config/mongoose.config';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';

@Module({
    imports: [
        MongooseModule.forRoot(process.env.MONGODB_URI || 'mongodb://localhost/auth', mongooseConfig),
        AuthModule,
        StreamsModule,
        UsersModule,
        ConfigModule.forRoot()
    ],
    controllers: [AppController]
})
export class AppModule { }
