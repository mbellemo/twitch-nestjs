import { Injectable, UnauthorizedException } from "@nestjs/common";
import { UsersRepository } from "../users/users.repository";
import { CreateUserDto } from "../users/dto/create-user.dto";
import { JwtPayload } from "./jwt-payload.interface";
import { JwtService } from "@nestjs/jwt";
import { User } from "../users/user.interface";

@Injectable()
export class AuthService {
    constructor(
        private readonly usersRepository: UsersRepository,
        private readonly jwtService: JwtService
    ) { }

    async signUp(createUserDto: CreateUserDto): Promise<User> {
        return await this.usersRepository.createUser(createUserDto);
    }

    async signIn(createUserDto: CreateUserDto): Promise<{ accessToken: String }> {
        const username = await this.usersRepository.validateUsernamePassword(createUserDto);

        if (!username) {
            throw new UnauthorizedException('Invalid credentials');
        }

        const payload: JwtPayload = { username };
        const accessToken = await this.jwtService.sign(payload);
        return { accessToken };
    }
}