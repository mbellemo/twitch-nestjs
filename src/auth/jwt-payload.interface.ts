export interface JwtPayload {
    readonly username: String;
}