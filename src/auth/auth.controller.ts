import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Query,
    UsePipes,
    ValidationPipe,
    UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { GetUser } from './get-user.decorator';
import { User } from '../users/user.interface';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) { }

    @Post('/signup')
    @UsePipes(ValidationPipe)
    async signUp(@Body() createUserDto: CreateUserDto): Promise<User> {
        return await this.authService.signUp(createUserDto);
    }

    @Post('/signin')
    @UsePipes(ValidationPipe)
    async signIn(@Body() createUserDto: CreateUserDto): Promise<{ accessToken: String }> {
        return await this.authService.signIn(createUserDto);
    }

    @Post('test')
    @UseGuards(AuthGuard())
    test(@GetUser() user: User) {
        console.log(user);
    }
}