import { Get, Controller } from "@nestjs/common";

@Controller()
export class AppController {
    constructor() { }

    @Get('hello')
    async getStream(): Promise<string> {
        return 'Hello';
    }
}