import { Stream } from './stream.interface';
import { StreamsRepository } from './streams.repository';
import { FindStreamDto } from './dto/find-stream.dto';
import { Injectable } from '@nestjs/common';
import { SearchStreamsDto } from './dto/search-streams.dto';

@Injectable()
export class StreamsService {
    constructor(private readonly streamsRepository: StreamsRepository) { }

    async getStreams(searchStreamsDto: SearchStreamsDto): Promise<any> {
        return await this.streamsRepository.getStreams(searchStreamsDto);
    }

    async getStreamById(findStreamDto: FindStreamDto): Promise<Stream> {
        return await this.streamsRepository.getStreamById(findStreamDto);
    }
}