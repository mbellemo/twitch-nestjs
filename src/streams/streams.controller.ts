import { Controller, Get, Param, ValidationPipe, UsePipes, Query } from '@nestjs/common';
import { Queue } from 'bull';
import { InjectQueue } from 'nest-bull';
import { StreamsService } from './streams.service';
import { FindStreamDto } from './dto/find-stream.dto';
import { Stream } from './stream.interface';
import { SearchStreamsDto } from './dto/search-streams.dto';

@Controller('streams')
export class StreamsController {
    constructor(private streamsService: StreamsService) { }

    @Get(':id')
    @UsePipes(ValidationPipe)
    async getStream(@Param('id') findStreamDto: FindStreamDto): Promise<Stream> {
        return await this.streamsService.getStreamById(findStreamDto);
    }

    @Get()
    @UsePipes(ValidationPipe)
    async getStreams(@Query() searchStreamsDto: SearchStreamsDto): Promise<any> {
        return await this.streamsService.getStreams(searchStreamsDto);
    }
}
