import { IsNotEmpty, IsString, IsOptional, IsNumber, IsPositive, IsDateString } from 'class-validator';

export class CreateStreamDto {
  @IsString()
  @IsNotEmpty()
  streamId: String;

  @IsString()
  @IsNotEmpty()
  userId: String;

  @IsString()
  @IsNotEmpty()
  userName: String;

  @IsString()
  @IsNotEmpty()
  gameId: String;

  @IsString()
  @IsOptional()
  gameName: String;

  @IsString()
  @IsNotEmpty()
  title: String;

  @IsPositive()
  @IsNotEmpty()
  viewerCount: number;

  @IsDateString()
  @IsNotEmpty()
  startedAt: String;

  @IsString()
  @IsNotEmpty()
  language: String;
}