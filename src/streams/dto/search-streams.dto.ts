import { IsString, IsIn, IsOptional, IsNumber, IsNumberString, IsInt } from "class-validator";
import { Transform } from "class-transformer"

export class SearchStreamsDto {
    @IsString()
    @IsIn(['startedAt', 'viewerCount'])
    @IsOptional()
    fieldSorted: String;

    @IsString()
    @IsIn(['asc', 'desc'])
    @IsOptional()
    directionSorted: String;

    @IsString()
    @IsOptional()
    next: String;

    @IsString()
    @IsOptional()
    previous: String;

    @IsInt()
    @IsOptional()
    @Transform(value => Number(value))
    limit: number = 10;

    @IsOptional()
    @IsString()
    gameId: String;

    @IsOptional()
    @IsString()
    userId: String;

    @IsOptional()
    @IsString()
    searchString: String;
}