import { Test, TestingModule } from '@nestjs/testing';
import { NotFoundException } from '@nestjs/common';
import { StreamsService } from './streams.service';
import { StreamsRepository } from './streams.repository';
import { FindStreamDto } from './dto/find-stream.dto';
import { SearchStreamsDto } from './dto/search-streams.dto';

const mockStream = {
  _id: "5e3sddb7ea996215f0bf9425c",
  streamId: "60555425",
  gameId: "49123931",
  language: "en",
  startedAt: "2020-02-08T10:03:13.000Z",
  title: "EFT GIVEAWAYS! TOP EDUCATIONAL STRIM",
  userId: "23732582",
  userName: "Sacral",
  viewerCount: 11613
};

const mockStreamList = [
  {
    _id: "5e3sddb7ea996215f0bf9425c",
    streamId: "60355425",
    gameId: "49124931",
    language: "de",
    startedAt: "2020-02-08T11:23:13.000Z",
    title: "VIP점프맵 켠왕 (욕할수도있음..)",
    userId: "23242582",
    userName: "악",
    viewerCount: 10623
  },
  {
    _id: "5e3sddb7ea996215f0bf9425c",
    streamId: "60555425",
    gameId: "49123931",
    language: "en",
    startedAt: "2020-02-08T10:03:13.000Z",
    title: "EFT GIVEAWAYS! TOP EDUCATIONAL STRIM",
    userId: "23732582",
    userName: "Sacral",
    viewerCount: 11613
  },
];

const mockStreamsRepository = () => ({
  getStreams: jest.fn(),
  getStreamById: jest.fn(),
});

describe('StreamService', () => {
  let streamsService: StreamsService;
  let streamsRepository: StreamsRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StreamsService,
        { provide: StreamsRepository, useFactory: mockStreamsRepository }
      ],
    }).compile();

    streamsService = await module.get<StreamsService>(StreamsService);
    streamsRepository = await module.get<StreamsRepository>(StreamsRepository);
  });

  it('should be defined', () => {
    expect(streamsService).toBeDefined();
  });

  describe('getStreams', () => {
    it('gets all streams from the repository', async () => {
      expect(streamsRepository.getStreams).not.toHaveBeenCalled();
      const spy = jest.spyOn(streamsRepository, 'getStreams')
        .mockImplementation(() => Promise.resolve(mockStreamList as any));
      const searchStreamsDto: SearchStreamsDto = {
        searchString: 'Sacral',
        fieldSorted: 'startedAt',
        directionSorted: 'asc',
        next: 'akdfhiubfihfiuhdshfsi',
        previous: 'akdfhiubfihfiuhdshfsi',
        limit: 10,
        gameId: '12345',
        userId: '123456'
      };
      const users = await streamsService.getStreams(searchStreamsDto);
      expect(streamsRepository.getStreams).toHaveBeenCalledWith(searchStreamsDto);
      expect(users).toEqual(mockStreamList);
    });
  });

  describe('getStreamById', () => {
    it('successfully retrieves a stream from the repository', async () => {
      expect(streamsRepository.getStreamById).not.toHaveBeenCalled();
      const findStreamDto: FindStreamDto = { id: '0' };
      const spy = jest.spyOn(streamsRepository, 'getStreamById')
        .mockImplementation(() => Promise.resolve(mockStreamList[0] as any));
      const stream = await streamsService.getStreamById(findStreamDto);
      expect(streamsRepository.getStreamById).toHaveBeenCalledWith(findStreamDto);
      expect(stream).toEqual(mockStreamList[0]);
    });

    it('throws an error as stream is not found', async () => {
      expect(streamsRepository.getStreamById).not.toHaveBeenCalled();
      const findStreamDto: FindStreamDto = { id: '0' };
      const spy = jest.spyOn(streamsRepository, 'getStreamById')
        .mockResolvedValue(null);
      expect(streamsService.getStreamById(findStreamDto)).rejects.toThrow(NotFoundException);
      expect(streamsRepository.getStreamById).toHaveBeenCalled();
    });

  });
});
