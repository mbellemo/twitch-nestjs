import { InjectModel, InjectConnection, MongooseModule } from "@nestjs/mongoose";
import { Stream } from "./stream.interface";
import { Model, Connection } from 'mongoose';
import * as MongoPaging from 'mongo-cursor-pagination';
import { NotFoundException, Injectable } from "@nestjs/common";
import { CreateStreamDto } from "./dto/create-stream.dto";
import { FindStreamDto } from "./dto/find-stream.dto";
import { SearchStreamsDto } from "./dto/search-streams.dto";

@Injectable()
export class StreamsRepository {
    constructor(@InjectModel('Stream') private readonly streamModel: Model<Stream>,
        @InjectConnection() private readonly connection: Connection) { }

    async getStreams(searchStreamsDto: SearchStreamsDto): Promise<any> {

        const {
            fieldSorted,
            directionSorted,
            next,
            previous,
            limit,
            gameId,
            userId,
            searchString
        } = searchStreamsDto;

        const paginatedField = fieldSorted === 'viewerCount' ? 'viewerCount' : 'startedAt';
        const sortAscending = directionSorted && directionSorted === 'asc' ? true : false;

        const query = (gameId || userId || searchString) ? {
            ...gameId && { gameId },
            ...userId && { userId },
            ...searchString && { '$text': { '$search': searchString } }
        } : null;

        const param = {
            ...query && { query },
            paginatedField,
            sortAscending,
            limit: limit || 10,
            ...next && { next },
            ...previous && { previous }
        };

        if (searchString) {
            await this.connection.db.collection('streams').createIndex({
                title: 'text',
                userName: 'text'
            },
                {
                    name: "streams_full_text",
                    default_language: "en",
                    language_override: "en"
                });
        }

        return await this.streamModel.paginate(param);
    }

    async getStreamById(findStreamDto: FindStreamDto): Promise<Stream> {
        const found = await this.streamModel.findById(findStreamDto).exec();
        if (!found) {
            throw new NotFoundException();
        }
        return found;
    }

    async findByStreamIdAndUpsert(streamDto: CreateStreamDto): Promise<Stream> {
        const { streamId } = streamDto;
        const options = { new: true, upsert: true };
        const found = await this.streamModel.findOneAndUpdate({ streamId: streamId }, streamDto, options).exec();
        if (!found) {
            throw new NotFoundException();
        }
        return found;
    }
}