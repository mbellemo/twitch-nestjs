import { Document } from 'mongoose';

export interface Stream extends Document {
    readonly streamId: String;
    readonly userId: String;
    readonly userName: String;
    readonly gameId: String;
    readonly title: String;
    readonly viewerCount: number;
    readonly startedAt: Date;
    readonly language: String;
}