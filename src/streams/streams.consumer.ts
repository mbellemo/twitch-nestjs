import {
    QueueProcessDecoratorOptions,
    Process,
    Processor
} from 'nest-bull';
import { Job } from 'bull';
import { Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Stream } from './stream.interface';
import { StreamsRepository } from './streams.repository';
import { CreateStreamDto } from './dto/create-stream.dto';

const queueProcessDecoratorOptions: QueueProcessDecoratorOptions = {
    name: 'streams'
};

@Processor()
export class StreamsConsumer {
    constructor(
        @InjectModel('Stream') private readonly streamModel: Model<Stream>,
        private readonly streamsRepository: StreamsRepository) { }

    private readonly logger = new Logger(StreamsConsumer.name);

    @Process(queueProcessDecoratorOptions)
    async consume(job: Job<any[]>) {
        let i = 0;
        for (i = 0; i < job.data.length; i++) {
            try {
                const streamDto: CreateStreamDto = this.streamModel.fromTwitch(job.data[i]);
                const stream = await this.streamsRepository.findByStreamIdAndUpsert(streamDto);
                this.logger.debug(streamDto, `Job-ID: ${job.id}, Twitch Stream: #${i.toString()}`);
                this.logger.log(stream, `Job-ID: ${job.id}, Saved Stream: #${i.toString()}`);
            } catch (error) {
                this.logger.error(error, `Job-ID: ${job.id}, Stream Error: #${i.toString()}`);
            } finally {
                job.progress(i);
            }
        }
        return {};
    }
}