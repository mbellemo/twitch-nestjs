import { Module } from '@nestjs/common';
import { StreamsConsumer } from './streams.consumer';
import { StreamsController } from './streams.controller';
import { StreamsService } from './streams.service';
import { BullModule } from 'nest-bull';
import { bullModuleOptions } from '../config/bull-module.config';
import { MongooseModule } from '@nestjs/mongoose';
import { StreamSchema } from './stream.schema';
import { StreamsRepository } from './streams.repository';

@Module({
    imports: [
        BullModule.register(bullModuleOptions),
        MongooseModule.forFeature([{ name: 'Stream', schema: StreamSchema }]),
    ],
    providers: [StreamsConsumer, StreamsService, StreamsRepository],
    controllers: [StreamsController]
})
export class StreamsModule { }