import * as mongoose from 'mongoose';
import * as MongoPaging from 'mongo-cursor-pagination';
import { Stream } from './stream.interface';
import { types } from 'util';

export const StreamSchema = new mongoose.Schema({
    streamId: { type: String, required: true, unique: true },
    userId: { type: String, required: true },
    userName: { type: String, required: true },
    gameId: { type: String, required: true },
    gameName: { type: String, required: false },
    title: { type: String, required: true },
    viewerCount: { type: Number, required: true },
    startedAt: { type: Date, required: true },
    language: { type: String, required: true },
});

StreamSchema.plugin(MongoPaging.mongoosePlugin);

StreamSchema.statics.fromTwitch = function (stream: any): any {
    const { id, user_id, user_name, game_id, title, viewer_count, started_at, language } = stream;
    return {
        streamId: id,
        userId: user_id,
        userName: user_name,
        gameId: game_id,
        title,
        viewerCount: Number.isSafeInteger(viewer_count) ? +viewer_count : 0,
        startedAt: Date.parse(started_at) !== NaN ? Date.parse(started_at) : null,
        language
    };
}